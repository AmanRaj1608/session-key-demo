// @ts-ignore
import { decode } from "cbor-x";

// Track the credentials that have been registered
var storedCredential: any = {};

// Webauthn config values
var is_prod = true;

// Local dev values
let rpName = "http://localhost:5173";
let rpId = "localhost";

if (is_prod) {
  // Prod values
  rpName = "https://session-key-demo.vercel.app";
  rpId = "session-key-demo.vercel.app";
}

// Placeholder
const userName = "user";

// Tokens that have been issued
var tokens: any = {};

function buf2hex(buffer: ArrayBuffer) {
  return [...new Uint8Array(buffer)]
    .map((x) => x.toString(16).padStart(2, "0"))
    .join("");
}

PublicKeyCredential.isUserVerifyingPlatformAuthenticatorAvailable()
  .then((available) => {
    if (available) {
      console.log("User has passkey functionality");
    } else {
      alert("Your device is not compatible with passkeys!");
    }
  })
  .catch((err) => {
    // Something went wrong
    console.error(err);
  });

/**
 * Asynchronously generates an authentication token for a Keygen process.
 *
 * This function initiates the generation of an instance ID for a Keygen process,
 * constructs a payload with the provided node count and threshold values, and
 * then proceeds to either register or log in, based on whether credentials are
 * already stored. It ensures that both the node count and threshold are greater
 * than 0 before proceeding with the token generation process.
 *
 * @param {number} nodeCount - The number of nodes to be used in the Keygen process.
 *                             Must be an integer greater than 0.
 * @param {number} threshold - The threshold value for the Keygen process.
 *                             Must be an integer greater than 0.
 * @param {number} timeoutValue - How long for the issued auth token to live.
 *                                Must be an integer greater than 0.
 *
 * @returns {Promise<string>} A promise that resolves to the generated authentication token.
 *
 * @throws {Error} If the nodeCount or threshold is less than 1, an alert is triggered
 *                 and the function returns early. If no token or challenge is returned
 *                 from the registration or login process, an error is thrown.
 *
 * @example
 * // Example usage:
 * getKeygenAuthToken(5, 3)
 *   .then(auth_token => {
 *     console.log('Auth Token:', auth_token);
 *   })
 *   .catch(error => {
 *     console.error('Error:', error);
 *   });
 */
export async function getKeygenAuthToken(
  nodeCount: number,
  threshold: number,
  timeoutValue?: number
): Promise<string> {
  console.log("Generating Keygen Instance ID...");

  if (typeof timeoutValue === "undefined") {
    timeoutValue = 60; // seconds
  }

  if (nodeCount < 1 || threshold < 1 || timeoutValue < 1) {
    // alert("Node count, threshold, and timeout value must be greater than 0");
    throw new Error("Inputs to getKeygenAuthtoken must be greater than 0");
  }

  var payload: any = {
    "sl-mpc-setup": {
      keygen: {
        n: nodeCount,
        t: threshold,
      },
    },
    timeout: timeoutValue,
  };

  console.log("Generated Payload:", payload);

  // If there is no stored credential, we need to register with the nodes
  if (Object.keys(storedCredential).length === 0) {
    const auth_token = await handleRegister(payload, nodeCount, threshold);
    if (!auth_token) {
      throw new Error("No token returned");
    }
    return auth_token;
  }

  const auth_token = await handleLogin(payload, nodeCount, threshold);
  if (!auth_token) {
    throw new Error("No challenge returned");
  }
  return auth_token;
}

/**
 * Asynchronously generates an authentication token for a signing process.
 *
 * This function constructs a payload with the provided message to sign, node count,
 * and threshold values for a signing process. It then proceeds to log in and
 * initiate the signing process using the constructed payload.
 *
 * @param {string} messageToSign - The message that needs to be signed as part of the authentication process.
 * @param {number} nodeCount - The number of nodes to be used in the signing process.
 *                             Must be an integer.
 * @param {number} threshold - The threshold value for the signing process.
 *                             Must be an integer.
 * @param {number} timeoutValue - How long for the issued auth token to live.
 *                                Must be an integer greater than 0.
 *
 * @returns {Promise<string>} A promise that resolves to the generated authentication token.
 *
 * @throws {Error} If no token or challenge is returned from the login process, an error is thrown.
 *
 * @example
 * // Example usage:
 * getSignAuthToken("Hello, World!", 5, 3)
 *   .then(token => {
 *     console.log('Auth Token:', token);
 *   })
 *   .catch(error => {
 *     console.error('Error:', error);
 *   });
 */
export async function getSignAuthToken(
  messageToSign: string,
  nodeCount: number,
  threshold: number,
  timeoutValue?: number
): Promise<string> {
  if (typeof timeoutValue === "undefined") {
    timeoutValue = 60; // seconds
  }

  var payload = {
    "sl-mpc-setup": {
      sign: {
        n: nodeCount,
        t: threshold,
        message: messageToSign,
      },
    },
    timeout: timeoutValue,
  };

  console.log("Generated Payload:", payload);

  // Assuming you have a function handleSignature to manage the signature process
  let token = await handleLogin(payload, nodeCount, threshold);
  if (!token) {
    throw new Error("No challenge returned");
  }
  return token;
}

/**
 * Asynchronously generates an authentication token for a signing process based on a regex pattern.
 *
 * This function constructs a payload with the provided regex pattern for the message to be signed,
 * along with node count, threshold values, and a timeout setting for the signing process. It then
 * proceeds to either register or log in, based on whether credentials are already stored, and
 * initiates the signing process using the constructed payload.
 *
 * @param {string} messagePattern - The regex pattern that the message to be signed must match.
 * @param {number} timeout - The timeout value in seconds for the signing process.
 * @param {number} nodeCount - The number of nodes to be used in the signing process.
 *                             Must be an integer.
 * @param {number} threshold - The threshold value for the signing process.
 *                             Must be an integer.
 *
 * @returns {Promise<string>} A promise that resolves to the generated authentication token.
 *
 * @throws {Error} If no token is returned from the registration or login process, an error is thrown.
 *
 * @example
 * // Example usage:
 * getRegexSignAuthToken("^[a-zA-Z0-9]+$", 60, 5, 3)
 *   .then(token => {
 *     console.log('Auth Token:', token);
 *   })
 *   .catch(error => {
 *     console.error('Error:', error);
 *   });
 */
export async function getRegexSignAuthToken(
  messagePattern: string,
  timeout: number,
  nodeCount: number,
  threshold: number
): Promise<string> {
  var payload = {
    "sl-mpc-setup": {
      regex_sign: {
        n: nodeCount,
        t: threshold,
        message_pattern: messagePattern,
      },
    },
    timeout: timeout,
  };

  console.log("Generated Payload:", payload);

  // If there is no stored credential, we need to register with the nodes
  if (Object.keys(storedCredential).length === 0) {
    const token = await handleRegister(payload, nodeCount, threshold);
    if (!token) {
      throw new Error("No token returned");
    }
    return token;
  }

  const token = await handleLogin(payload, nodeCount, threshold);
  if (!token) {
    throw new Error("No challenge returned");
  }
  return token;
}

/**
 * Asynchronously handles the registration process with multiple nodes for authentication.
 *
 * This function performs several steps to register with a distributed authentication system:
 * 1. Fetches challenges from each node's registration challenge endpoint.
 * 2. Constructs a setup object with the fetched challenges and the current time.
 * 3. Generates a SHA-256 hash of the setup object to use as a final challenge.
 * 4. Creates a new credential using the Web Authentication API (WebAuthn).
 * 5. Validates the registration with each node.
 * 6. Saves the credential for later use if registration is successful.
 *
 * @param {any} setup - The initial setup object provided for registration.
 * @param {number} nodeCount - The number of nodes to register with.
 * @param {number} threshold - The threshold value for the registration process.
 *
 * @returns {Promise<string>} A promise that resolves to the token received from the registration process.
 *
 * @throws {Error} Throws an error if:
 *                 - Fetching challenges from any node fails.
 *                 - No credential is returned from the WebAuthn API.
 *                 - The attestation object is not an ArrayBuffer.
 *                 - Validation with any node fails.
 *                 - Registration responses from nodes do not match.
 *
 * @example
 * // Example usage:
 * handleRegister(someSetupObject, 5, 3)
 *   .then(token => {
 *     console.log('Registration Token:', token);
 *   })
 *   .catch(error => {
 *     console.error('Registration Error:', error);
 *   });
 */
export async function handleRegister(
  setup: any,
  nodeCount: number,
  threshold: number
): Promise<string> {
  // TODO: Break up this massive function!!
  console.log("Registering with nodes...");

  // Get the challenges from the Node URLs
  const nodeURLs = [
    "https://sl-dkls23-passkeys.fly.dev/auth-0/",
    "https://sl-dkls23-passkeys.fly.dev/auth-1/",
    "https://sl-dkls23-passkeys.fly.dev/auth-2/",
  ];

  // for (let i = 0; i < nodeCount; i++) {
  //   nodeURLs.push(`/auth-${i}/`);
  // }

  // Fetch the challenges from the `issue_registration_challenge` endpoint of each node
  const challengePromises = nodeURLs.map((url) => {
    return fetch(`${url}/issue_registration_challenge`, {
      method: "GET",
    })
      .then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw new Error(`Error fetching challenge from ${url}`);
        }
      })
      .then((json) => {
        return json.challenge;
      });
  });

  console.log("Fetching challenges from nodes...");
  const challenges = await Promise.all(challengePromises);
  console.log("Challenges:");
  console.log(challenges);

  const timeNow = new Date();

  setup["challenges"] = challenges;
  setup["time"] = timeNow;

  // Sort the challenges alphabetically, concatenate them, and use the SHA-256 hash as the final challenge
  const setupString = JSON.stringify(setup);
  const challenge = await crypto.subtle.digest(
    "SHA-256",
    new TextEncoder().encode(setupString)
  );
  console.log("Challenge:");
  console.log(arrayBufferToBase64Url(challenge));

  const options: CredentialCreationOptions = {
    publicKey: {
      authenticatorSelection: {
        authenticatorAttachment: "platform",
        residentKey: "preferred",
        requireResidentKey: false,
      },
      challenge: challenge,
      excludeCredentials: [],
      pubKeyCredParams: [
        // For now, only support basic keys
        { type: "public-key", alg: -7 },
        { type: "public-key", alg: -257 },
      ],
      rp: {
        name: rpName,
        id: rpId,
      },
      user: {
        name: userName,
        displayName: userName,
        id: urlSafeBase64Encode(userName),
      },
    },
  };

  console.log("Options:");
  console.log(options);

  try {
    const my_cred = await navigator.credentials.create(options);
    console.log("Credentials:");
    console.log(my_cred);

    if (my_cred === null) {
      throw new Error("No credential returned");
    }

    // convert the credentials to a JSON string
    const my_cred_json = JSON.stringify({
      // @ts-ignore
      authenticatorAttachment: my_cred.authenticatorAttachment,
      id: my_cred.id,
      // @ts-ignore
      rawId: arrayBufferToBase64Url(my_cred.rawId),
      response: {
        attestationObject: arrayBufferToBase64Url(
          // @ts-ignore
          my_cred.response.attestationObject
        ),
        // @ts-ignore
        clientDataJSON: arrayBufferToBase64Url(my_cred.response.clientDataJSON),
      },
      type: my_cred.type,
    });
    console.log("As JSON:");
    console.log(JSON.parse(my_cred_json));

    // decode the clientDataJSON into a utf-8 string
    const utf8Decoder = new TextDecoder("utf-8");
    const decodedClientData = utf8Decoder.decode(
      // @ts-ignore
      my_cred.response.clientDataJSON
    );

    // parse the string as an object
    const clientDataObj = JSON.parse(decodedClientData);

    console.log("Client Data Object:");
    console.log(clientDataObj);

    // note: a CBOR decoder library is needed here.
    // Check if my_cred.response.attestationObject is an ArrayBuffer
    // @ts-ignore
    if (!(my_cred.response.attestationObject instanceof ArrayBuffer)) {
      throw new Error(
        "Invalid attestation object:",
        // @ts-ignore
        my_cred.response.attestationObject
      );
    }

    // Convert ArrayBuffer to Uint8Array
    // @ts-ignore
    const uint8Array = new Uint8Array(my_cred.response.attestationObject);

    // Decode the Uint8Array
    const decodedAttestationObject = decode(uint8Array);

    console.log("Decoded Attestation Object:", decodedAttestationObject);

    console.log("Attestation Object:");
    console.log(decodedAttestationObject);

    const { authData } = decodedAttestationObject;

    // get the length of the credential ID
    const dataView = new DataView(new ArrayBuffer(2));
    const idLenBytes = authData.slice(53, 55);
    idLenBytes.forEach((value: number, index: number) =>
      dataView.setUint8(index, value)
    );
    const credentialIdLength = dataView.getUint16(0);

    // get the credential ID
    const credentialId = authData.slice(55, 55 + credentialIdLength);
    console.log("Credential ID:");
    console.log(arrayBufferToBase64Url(credentialId));

    // Ask each node to check the registration
    const registrationPromises = nodeURLs.map((url) => {
      return fetch(`${url}/verify_registration`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          raw_credential: my_cred_json,
          setup_string: setupString,
          origin: rpName,
          rp_id: rpId,
        }),
      }).then((response) => {
        if (response.status === 200) {
          // Change my indicator to a checkmark
          console.log("Registered ok!");
        } else {
          console.error(
            `Error validating registration with ${url}`,
            response.json()
          );
          throw new Error(`Error validating registration with ${url}`);
        }
        return response.json();
      });
    });
    console.log("Validating registration with nodes...");
    const registrationResponses = await Promise.all(registrationPromises);
    console.log("Registration responses:");
    console.log(registrationResponses);

    // Check that all nodes returned the same JSON
    const firstResponse = registrationResponses[0];
    for (let i = 1; i < registrationResponses.length; i++) {
      const response = registrationResponses[i];
      if (response.challenge_hex !== firstResponse.challenge_hex) {
        console.error("Registration response challenge hexes do not match.");
        console.error(response.challenge_hex);
        console.error(firstResponse.challenge_hex);
        throw new Error("Mismatched responses from nodes!");
      }
    }

    // save the credential for later use
    storedCredential = JSON.parse(firstResponse.credential);
    console.log("Credentials is now:", storedCredential);

    return firstResponse.token;
  } catch (error) {
    console.error("Error creating credentials:", error);
    return "";
  }
}

export async function handleLogin(
  setup: any,
  nodeCount: number,
  threshold: number
) {
  // TODO: Break up this massive function!!
  if (!storedCredential) {
    console.error("No credentials selected.");
    throw new Error("No credentials selected.");
  } else {
    console.log("Using credential:", storedCredential);
  }
  console.log(storedCredential.credential_id);
  const credentialId = base64UrlToArrayBuffer(storedCredential.credential_id);
  console.log("Credential ID:", credentialId);

  // Decode the credential ID from base64Url to ArrayBuffer

  // Fetch the nonces from the `issue_authentication_challenge` endpoint of each node
  const nodeURLs = [
    "https://sl-dkls23-passkeys.fly.dev/auth-0/",
    "https://sl-dkls23-passkeys.fly.dev/auth-1/",
    "https://sl-dkls23-passkeys.fly.dev/auth-2/",
  ];

  // for (let i = 0; i < nodeCount; i++) {
  //   nodeURLs.push(`/auth-${i}/`);
  // }

  const challengePromises = nodeURLs.map((url) => {
    return fetch(
      `${url}/issue_authentication_challenge?credential_id=${storedCredential.credential_id}`,
      {
        method: "GET",
      }
    ).then((response) => {
      return response.json();
    });
  });

  console.log("Fetching challenges from nodes...");
  const node_responses = await Promise.all(challengePromises);
  console.log("Challenges:", node_responses);
  // Check that all nodes did not return an error
  let node_challenges: any = {};
  for (let i = 0; i < node_responses.length; i++) {
    const challenge = node_responses[i];

    if (Object.keys(challenge).includes("error")) {
      console.error(`Error fetching challenge from ${nodeURLs[i]}.`);
      throw new Error(`Error fetching challenge from ${nodeURLs[i]}`);
    }
    node_challenges[nodeURLs[i]] = challenge.challenge;
  }

  const timeNow = new Date();
  console.log("Time now:");
  console.log(timeNow);

  setup["nonces"] = node_challenges;
  setup["time"] = timeNow;
  if (setup["timeout"] === undefined) {
    setup["timeout"] = 30;
  }

  // // Update the Json input with the new data
  // document.getElementById('jsonInput').value = JSON.stringify(setup, undefined, 4);

  // The challenge is the SHA-256 hash of the JSON string
  const challenge = await crypto.subtle.digest(
    "SHA-256",
    new TextEncoder().encode(JSON.stringify(setup))
  );
  console.log("Challenge");
  console.log(arrayBufferToBase64Url(challenge));
  const challenge_hex = buf2hex(challenge);
  console.log("Challenge hex", challenge_hex);

  let options: any = {
    publicKey: {
      challenge: challenge,
      allowCredentials: [
        {
          type: "public-key",
          id: credentialId,
        },
      ],
      attestation: "none",
      attestationFormats: [],
    },
  };

  try {
    console.log("Options:");
    console.log(options);
    const assertion: any = await navigator.credentials.get(options);
    console.log("Assertion:");
    console.log(assertion);

    console.log("Authenticator Data:");
    console.log(assertion.response.authenticatorData);

    // Convert the credentials to a JSON string
    const assertionJson = JSON.stringify({
      authenticatorAttachment: assertion.authenticatorAttachment,
      id: assertion.id,
      rawId: arrayBufferToBase64Url(assertion.rawId),
      response: {
        authenticatorData: arrayBufferToBase64Url(
          assertion.response.authenticatorData
        ),
        clientDataJSON: arrayBufferToBase64Url(
          assertion.response.clientDataJSON
        ),
        signature: arrayBufferToBase64Url(assertion.response.signature),
        userHandle: arrayBufferToBase64Url(assertion.response.userHandle),
      },
      type: assertion.type,
    });

    console.log("Using the public key:");
    console.log(storedCredential.credential_public_key);
    console.log("----------", setup);
    // Ask each node to check the verification
    const verificationPromises = nodeURLs.map((url) => {
      return fetch(`${url}/verify_authentication`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          setup_string: JSON.stringify(setup),
          setup: setup,
          assertion: assertionJson,
          challenge: arrayBufferToBase64Url(challenge),
          origin: rpName,
          rp_id: rpId,
          credential_id: storedCredential.credential_id,
          sign_count: storedCredential.sign_count,
        }),
      }).then((response) => {
        if (response.status === 200) {
          return response.json();
        } else {
          console.error(
            `Error validating authentication with ${url}`,
            response.json()
          );
          throw new Error(`Error validating authentication with ${url}`);
        }
      });
    });
    console.log("Validating authentication with nodes...");
    const verificationResponses = await Promise.all(verificationPromises);
    console.log("Verification responses:");
    console.log(verificationResponses);

    // TODO: Update the sign count (I DONT GET THIS???)

    // Each node will have returned a JWT, store those in a dictionary
    for (let i = 0; i < verificationResponses.length; i++) {
      const response = verificationResponses[i];

      if (!tokens[nodeURLs[i]]) {
        console.log(
          "Creating new entry in tokens dict",
          storedCredential.credential_id
        );
        tokens[nodeURLs[i]] = {
          [storedCredential.credential_id]: response.token,
          // [storedCredential.credential_id]: challenge_hex,
        };
      } else {
        tokens[nodeURLs[i]][storedCredential.credential_id] = response.token;
        // tokens[nodeURLs[i]][storedCredential.credential_id] = challenge_hex;
      }
    }
    return verificationResponses[0].token;
  } catch (error) {
    console.error("Error during authentication:", error);
    return;
  }
}

function urlSafeBase64Encode(value: string) {
  let encoder = new TextEncoder();
  let data = encoder.encode(value);
  return data.buffer;
}

function base64UrlToArrayBuffer(base64Url: string) {
  let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
  let binaryString = atob(base64);
  let len = binaryString.length;
  let bytes = new Uint8Array(len);

  for (let i = 0; i < len; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }

  return bytes.buffer;
}

function arrayBufferToBase64Url(arrayBuffer: ArrayBuffer) {
  let bytes = new Uint8Array(arrayBuffer);
  let binaryString = "";

  for (let i = 0; i < bytes.byteLength; i++) {
    binaryString += String.fromCharCode(bytes[i]);
  }

  let base64 = btoa(binaryString);
  return base64.replace(/\+/g, "-").replace(/\//g, "_").replace(/=+$/, "");
}
