import { useEffect, useState } from "react";
import { makeStyles } from "@mui/styles";
import { CircularProgress } from "@mui/material";
import { ToastContainer } from "react-toastify";
import { v4 as uuidv4 } from "uuid";
import { useAccount } from "wagmi";
import { VoidSigner, ethers } from "ethers";
import {
  concat,
  defaultAbiCoder,
  hexlify,
  toUtf8Bytes,
} from "ethers/lib/utils";
// components
import Navbar from "@/components/Navbar";
import Footer from "@/components/Footer";
import { useSmartAccountContext } from "@/context/SmartAccountContext";
import { getKeygenAuthToken, handleLogin } from "@/context/passkey";
import { configInfo, showErrorMessage, showSuccessMessage } from "@/util";
import { configs, encodeHex } from "@/util/config";
import { useEthersSigner } from "@/context/ethers";
// biconomy sdk
import {
  DEFAULT_SESSION_KEY_MANAGER_MODULE,
  SessionKeyManagerModule,
} from "@biconomy-devx/modules";
import { UserOperation } from "@biconomy-devx/core-types";
import { PaymasterMode } from "@biconomy-devx/paymaster";

const PasskeysCont = () => {
  const classes = useStyles();
  const { address } = useAccount();
  const signer = useEthersSigner();
  const { smartAccount, scwAddress } = useSmartAccountContext();
  const [message, setMessage] = useState("🚀 Create a new session →");
  const [isLoading, setIsLoading] = useState(false);
  const [isSendErcLoading, setIsSendErcLoading] = useState(false);
  const [sessionEoaId, setSessionEoaId] = useState("");
  const [isSessionKeyModuleEnabled, setIsSessionKeyModuleEnabled] =
    useState(false);

  const [resp_global, setResp_global] = useState<any>({});

  useEffect(() => {
    let checkSessionModuleEnabled = async () => {
      if (!scwAddress || !smartAccount || !address) {
        setIsSessionKeyModuleEnabled(false);
        return;
      }
      try {
        let biconomySmartAccount = smartAccount;
        const isEnabled = await biconomySmartAccount.isModuleEnabled(
          DEFAULT_SESSION_KEY_MANAGER_MODULE
        );
        console.log("isSessionKeyModuleEnabled", isEnabled);
        setIsSessionKeyModuleEnabled(isEnabled);
        return;
      } catch (err: any) {
        console.error(err);
        setIsLoading(false);
        // showErrorMessage("Error in getting session key module status");
        setIsSessionKeyModuleEnabled(false);
        return;
      }
    };
    checkSessionModuleEnabled();
  }, [isSessionKeyModuleEnabled, scwAddress, smartAccount, address]);

  /**
   * Handle setting up and initiating a distributed key generation session, which makes use of the cloud nodes.
   *
   * This function does the following:
   *   1. Define a keygen setup
   *   2. Register/Authorise the user to the nodes, using Passkeys
   *   3. Upload the setup and authorisation to the nodes
   *   4. Wait for the resulting public key. Return the public key.
   */
  const handleGenKeys = async () => {
    try {
      let cluster = configs()[1]; // TODO provide UI to select a cluster
      let threshold = 2;
      let ttl = 10;
      const nodeCount = cluster.nodes.length; // 3
      const auth_token = await getKeygenAuthToken(nodeCount, threshold);

      const array = new Uint8Array(32);
      const instance = window.crypto.getRandomValues(array);
      console.log("Auth token: ", auth_token);
      console.log("Instance ID: ", instance);

      console.log("Instance base64: ", encodeHex(instance));
      console.log("I am encoding my signing key: ", cluster.setup.secretKey);

      const opts = {
        setup: {
          auth_token,
          t: threshold,
          parties: cluster.nodes.map((n: any) => {
            console.log("Public key is getting encoded: ", n.publicKey);
            return { rank: 0, public_key: encodeHex(n.publicKey) };
          }),
        },
        instance: encodeHex(instance),
        signing_key: encodeHex(cluster.setup.secretKey),
        ttl,
      };
      console.log("Using options: ", opts);

      let resp = await Promise.all(
        cluster.nodes.map(async (n: any) => {
          try {
            let resp = await fetch(
              `https://sl-dkls23-passkeys.fly.dev${n.endpoint}/v1/keygen`,
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  instance: encodeHex(instance),
                  opts,
                }),
              }
            );
            if (resp.status !== 200) throw new Error("Error in fetching");
            const json = await resp.json();
            return json;
          } catch (error: any) {
            console.error(error);
            throw error;
          }
        })
      );
      // resp_global = resp;
      setResp_global(resp);

      console.log("resp: ", resp);
      let generatedPublicKey = resp[0].public_key;
      return generatedPublicKey;
    } catch (error) {
      console.error(error);
      return null;
    }
  };

  const createSession = async (enableSessionKeyModule: boolean) => {
    if (!scwAddress || !smartAccount || !address) {
      showErrorMessage("Please connect wallet first");
      return;
    }
    try {
      setIsLoading(true);
      setMessage("🔑 Generating a new pkey pair...");

      const challengeId = uuidv4();
      console.log("challengeId", challengeId);
      // Generating a new pkey pair with dkls
      const sessionEOACompressed = await handleGenKeys();
      const sessionEOA = ethers.utils.computeAddress(
        "0x" + sessionEOACompressed
      );
      console.log("sessionEOA", sessionEOA);
      setSessionEoaId(sessionEOA);

      setMessage("Setting session key on chain...");

      let biconomySmartAccount = smartAccount;
      // generate sessionModule
      const sessionModule = await SessionKeyManagerModule.create({
        moduleAddress: DEFAULT_SESSION_KEY_MANAGER_MODULE,
        smartAccountAddress: scwAddress,
      });

      // cretae session key data
      const sessionKeyData = defaultAbiCoder.encode(
        ["address", "address", "address", "uint256"],
        [
          sessionEOA,
          configInfo.usdc.address, // erc20 token address
          "0x42138576848E839827585A3539305774D36B9602", // receiver address
          ethers.utils.parseUnits("50".toString(), 18).toHexString(), // 5 usdc amount
        ]
      );
      const erc20ModuleAddr = "0x000000D50C68705bd6897B2d17c7de32FB519fDA";
      const sessionTxData = await sessionModule.createSessionData([
        {
          validUntil: 0,
          validAfter: 0,
          sessionValidationModule: erc20ModuleAddr,
          sessionPublicKey: sessionEOA,
          sessionKeyData: sessionKeyData,
        },
        // can optionally enable multiple leaves(sessions) altogether
      ]);
      console.log("sessionTxData", sessionTxData);

      // tx to set session key
      const tx2 = {
        to: DEFAULT_SESSION_KEY_MANAGER_MODULE, // session manager module address
        data: sessionTxData.data,
      };

      let transactionArray = [];
      if (enableSessionKeyModule) {
        // -----> enableModule session manager module
        const tx1 = await biconomySmartAccount.getEnableModuleData(
          DEFAULT_SESSION_KEY_MANAGER_MODULE
        );
        transactionArray.push(tx1);
      }
      transactionArray.push(tx2);
      // tx3 -> mint usdc
      const tokenContract = new ethers.Contract(
        configInfo.usdc.address,
        configInfo.usdc.abi,
        signer
      );
      const { data } = await tokenContract.populateTransaction.mint(
        await biconomySmartAccount.getAccountAddress(),
        ethers.utils.parseUnits("100".toString(), 18)
      );
      const tx3 = {
        to: configInfo.usdc.address, //erc20 token address
        data: data,
        value: "0",
      };
      transactionArray.push(tx3);

      let partialUserOp = await biconomySmartAccount.buildUserOp(
        transactionArray,
        {
          paymasterServiceData: {
            mode: PaymasterMode.SPONSORED,
          },
        }
      );

      const userOpResponse = await smartAccount.sendUserOp(partialUserOp);
      console.log("userOpHash", userOpResponse);
      const { transactionHash } = await userOpResponse.waitForTxHash();
      console.log("txHash", transactionHash);
      showSuccessMessage(
        `Session Created Successfully ${transactionHash}`,
        transactionHash
      );

      setIsLoading(false);
      setMessage("🎉 Session created successfully!");
    } catch (error: any) {
      console.error("error", error);
      setIsLoading(false);
      setMessage("🚨 Create a new session →");
      showErrorMessage(error.message);
    }
  };

  const handleSignGen = async (signMessage: string) => {
    try {
      let cluster = configs()[1]; // TODO provide UI to select a cluster
      let threshold = 2;
      const nodeCount = cluster.nodes.length; // 3
      const array = new Uint8Array(32);
      const instance = window.crypto.getRandomValues(array);
      const timeoutValue = 60 * 60; // seconds

      var payload = {
        "sl-mpc-setup": {
          sign: {
            n: nodeCount,
            t: threshold,
            message: signMessage.slice(2),
          },
        },
        timeout: timeoutValue,
      };
      console.log("Generated Payload:", payload);
      console.log("----------", payload);
      // Assuming you have a function handleSignature to manage the signature process
      let auth_token = await handleLogin(payload, nodeCount, threshold);
      if (!auth_token) {
        throw new Error("No challenge returned");
      }
      console.log("imp>signMessage", signMessage);
      let opts = {
        setup: {
          auth_token,
          parties: cluster.nodes.slice(0, threshold).map((n) => {
            return { rank: 0, public_key: encodeHex(n.publicKey) };
          }),
          message: signMessage.slice(2),
          raw_message: signMessage,
          public_key: resp_global[0].public_key,
          hash_algo: "keccak256",
        },
        instance: encodeHex(instance),
        signing_key: encodeHex(cluster.setup.secretKey),
        ttl: 10,
      };
      console.log("Using options: ", opts);

      let resp = await Promise.all(
        cluster.nodes.slice(0, threshold).map(async (n) => {
          try {
            let resp = await fetch(
              `https://sl-dkls23-passkeys.fly.dev${n.endpoint}/v1/signgen`,
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify({
                  instance: encodeHex(instance),
                  opts,
                }),
              }
            );
            if (resp.status !== 200) throw new Error("Error in fetching");
            const json = await resp.json();
            return json;
          } catch (error: any) {
            console.error(error);
            throw error;
          }
        })
      );

      console.log("resp: ", resp);
      let signature = resp[0].sign;
      return signature;
    } catch (err: any) {
      console.error(err);
    }
  };

  const sendErc20 = async () => {
    if (!scwAddress || !smartAccount || !address) {
      showErrorMessage("Please connect wallet first");
      return;
    }
    try {
      setIsSendErcLoading(true);
      let biconomySmartAccount = smartAccount;
      const erc20ModuleAddr = "0x000000D50C68705bd6897B2d17c7de32FB519fDA";

      // generate sessionModule
      const sessionModule = await SessionKeyManagerModule.create({
        moduleAddress: DEFAULT_SESSION_KEY_MANAGER_MODULE,
        smartAccountAddress: scwAddress,
      });

      // set active module to sessionModule
      biconomySmartAccount =
        biconomySmartAccount.setActiveValidationModule(sessionModule);

      const tokenContract = new ethers.Contract(
        configInfo.usdc.address,
        configInfo.usdc.abi,
        signer
      );
      let decimals = 18;

      try {
        decimals = await tokenContract.decimals();
      } catch (error) {
        throw new Error("invalid token address supplied");
      }
      const { data } = await tokenContract.populateTransaction.transfer(
        "0x42138576848E839827585A3539305774D36B9602", // receiver address
        ethers.utils.parseUnits("5".toString(), decimals)
      );
      const tx1 = {
        to: configInfo.usdc.address, //erc20 token address
        data: data,
        value: "0",
      };

      const sessionSigner = new VoidSigner(sessionEoaId, signer!.provider);
      console.log("sessionSigner address", await sessionSigner.getAddress());
      // build user op
      let userOp = await biconomySmartAccount.buildUserOp([tx1], {
        overrides: {
          verificationGasLimit: "150000",
        },
        // skipBundlerGasEstimation: false,
        paymasterServiceData: {
          mode: PaymasterMode.SPONSORED,
        },
        params: {
          sessionSigner: sessionSigner,
          sessionValidationModule: erc20ModuleAddr,
        },
      });
      console.log("userOp", userOp);

      const userOpHash = await biconomySmartAccount.getUserOpHash(userOp);
      console.log("imp>userOpHash", userOpHash);

      const messagePrefix = "\x19Ethereum Signed Message:\n";
      const myMessage = ethers.utils.arrayify(userOpHash);
      const hashMessage = concat([
        toUtf8Bytes(messagePrefix),
        toUtf8Bytes(String(myMessage.length)),
        myMessage,
      ]);

      console.log("imp>hashMessage", hexlify(hashMessage));
      let sigUserOpHash = await handleSignGen(hexlify(hashMessage));

      console.log("imp>prev sigUserOpHash", sigUserOpHash);

      // 01-> 1c,00-> 1b
      if (sigUserOpHash.endsWith("01")) {
        sigUserOpHash = sigUserOpHash.slice(0, -2) + "1c";
      } else {
        sigUserOpHash = sigUserOpHash.slice(0, -2) + "1b";
      }
      console.log("imp>sigUserOpHash", sigUserOpHash);

      let finalSig = await sessionModule.signUserOpHash(userOpHash, {
        sessionSigner: sessionSigner,
        sessionValidationModule: erc20ModuleAddr,
        signature: "0x" + sigUserOpHash,
      } as any);

      finalSig = ethers.utils.defaultAbiCoder.encode(
        ["bytes", "address"],
        [finalSig, DEFAULT_SESSION_KEY_MANAGER_MODULE]
      );

      // send user op
      userOp.signature = finalSig;
      console.log("userOp", userOp);
      const userOpResponse = await biconomySmartAccount.sendSignedUserOp(
        userOp as UserOperation,
        {
          simulationType: "validation",
        }
      );
      console.log("userOpHash", userOpResponse);
      const { transactionHash } = await userOpResponse.waitForTxHash();
      console.log("txHash", transactionHash);
      showSuccessMessage(`ERC20 Transfer ${transactionHash}`, transactionHash);
      setIsSendErcLoading(false);
    } catch (error: any) {
      console.error(error);
      setIsSendErcLoading(false);
      showErrorMessage(error.message);
    }
  };

  return (
    <div className={classes.bgCover}>
      <Navbar />
      <div style={{ borderTop: "2px solid #282b4c" }}></div>
      <div className={classes.market}>
        <div style={{ height: 10 }}></div>
        <div className={classes.cont_img}>
          <img
            src="img/logo-cent.svg"
            alt="main"
            className={classes.logo}
            draggable="false"
          />

          <img
            src="img/silence-logo.png"
            alt="main"
            className={classes.logo}
            draggable="false"
          />
        </div>

        {/* <div className={classes.contText}>Biconomy + Silence Labs</div> */}
        <div className={classes.contText2}>
          We partnered together to solve the session key management problem for
          Smart Accounts. Demo
          <br /> for creating Session Key and transfer ERC20 from the smart
          accounts using that session key.
        </div>

        <div
          style={{
            width: "100%",
            margin: "auto",
            padding: "0 20px",
          }}
        >
          <button
            onClick={() => createSession(!isSessionKeyModuleEnabled)}
            className={classes.btn}
            style={{
              margin: "auto",
              marginTop: "3%",
            }}
          >
            {isLoading && (
              <CircularProgress
                sx={{
                  color: "white",
                  marginRight: 2,
                }}
                size={20}
              />
            )}
            {message}
          </button>

          {scwAddress && (
            <div
              style={{
                margin: "0 auto",
                marginTop: 30,
                maxWidth: 750,
                wordBreak: "break-all",
                backgroundColor: "rgb(7, 39, 35, 0.2)",
                padding: "20px",
              }}
            >
              <h4>Smart Account: </h4>
              <li>
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href={`https://mumbai.polygonscan.com/address/${scwAddress}`}
                >
                  <span style={{ color: "green" }}>{scwAddress}</span>
                </a>
              </li>
            </div>
          )}

          {sessionEoaId && (
            <div
              style={{
                margin: "0 auto",
                marginTop: 30,
                maxWidth: 750,
                wordBreak: "break-all",
                backgroundColor: "rgb(7, 39, 35, 0.2)",
                padding: "20px",
              }}
            >
              <h4>Session Key generated: </h4>
              <li>
                <span style={{ color: "green" }}>{sessionEoaId}</span>
              </li>
            </div>
          )}

          <button
            onClick={() => sendErc20()}
            className={classes.btn}
            style={{
              margin: "auto",
              marginTop: "3%",
            }}
          >
            {isSendErcLoading && (
              <CircularProgress
                sx={{
                  color: "white",
                  marginRight: 2,
                }}
                size={20}
              />
            )}
            📨 Send USDC using Session Key
          </button>
        </div>
      </div>
      <Footer />
      <ToastContainer position="bottom-left" newestOnTop theme="dark" />
    </div>
  );
};

const useStyles = makeStyles(() => ({
  bgCover: {
    fontFamily: "'Roboto', sans-serif",
    fontWeight: 500,
    backgroundColor: "#f6fefd",
    opacity: 0.9,
    background: "url(/img/background.svg) center 71px / auto repeat",
    // backgroundSize: "cover",
  },
  market: {
    maxWidth: 1000,
    minHeight: "80vh",
    margin: "10px auto",
  },
  cont_img: {
    maxWidth: 400,
    marginTop: 40,
    padding: "0 20px",
    display: "flex",
    justifyContent: "space-between",
    margin: "auto",
    alignItems: "center",
    alignContent: "center",
  },
  logo: {
    width: "150px",
    height: "100%",
  },
  contTitle: {
    fontFamily: "Work Sans",
    fontSize: 50,
    letterSpacing: "-0.01em",
    color: "rgb(25, 56, 51)",
    textAlign: "center",

    "@media (max-width:659px)": {
      fontSize: 30,
    },
  },
  contText: {
    margin: "8px auto",
    whiteSpace: "nowrap",
    fontFamily: "Inter",
    fontWeight: 500,
    fontSize: 28,
    marginTop: 20,
    color: "rgba(25, 56, 51, 0.9)",
    textAlign: "center",
  },
  contText2: {
    marginTop: 40,
    margin: "15px auto",
    // whiteSpace: "nowrap",
    fontFamily: "Inter",
    fontWeight: 500,
    fontSize: 15,
    color: "rgba(25, 56, 51, 0.7)",
    textAlign: "center",
  },
  btn: {
    margin: "20px 0 20px 40%",
    background: "rgb(40,43,76, 0.9)",
    cursor: "pointer",
    border: 0,
    outline: "none",
    borderRadius: 5,
    height: "36px",
    fontSize: 18,
    lineHeight: "36px",
    padding: "0 18px 0 18px",
    borderBottom: "1px solid #000",
    display: "flex",
    alignItems: "center",
    color: "white",

    "@media (max-width:599px)": {
      padding: 0,
    },

    "&:hover": {
      backgroundColor: "rgb(40,43,76, 0.8)",
    },

    "& div": {
      "@media (max-width:599px)": {
        margin: 0,
        display: "none",
      },
    },
  },
}));

export default PasskeysCont;
