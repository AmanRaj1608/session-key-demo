import { makeStyles } from "@mui/styles";

const Footer = () => {
  const classes = useStyles();

  return (
    <footer
      className={classes.footer}
      style={{
        backgroundColor: "#FF5B22",
      }}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          padding: "0px 20px",
          maxWidth: 1280,
          margin: "auto",
        }}
      >
        <div
          style={{
            margin: "auto",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <img src="/img/logo-black.svg" alt="logo" className={classes.logo} />
          <p className={classes.text}>© 2023 Biconomy. All rights reserved</p>
        </div>
      </div>
    </footer>
  );
};

const useStyles = makeStyles((theme) => ({
  ...theme,
  footer: {
    position: "relative",
    borderTop: "2px solid #FF9130",
    display: "block",
    padding: "25px 0 50px 0",
  },
  logo: {
    height: "40px",
    margin: "auto",
    // marginRight: "10px",
  },
  text: {
    color: "#000",
    fontSize: "16px",
    fontWeight: 300,
    marginBottom: 0,
  },
}));

export default Footer;
