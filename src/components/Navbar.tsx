// import { NavLink } from "react-router-dom";
import { AppBar, Container } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { ConnectButton } from "@rainbow-me/rainbowkit";

const Navbar = () => {
  const classes = useStyles();

  return (
    <AppBar position="static" classes={{ root: classes.nav }}>
      <Container className={classes.container}>
        <div className={classes.flexContainer}>
          {/* <NavLink style={{ display: "flex", alignItems: "center" }} to="/"> */}
            <img src="/img/logo-hor.svg" alt="logo" className={classes.logo} />
          {/* </NavLink> */}
          <ConnectButton />
        </div>
      </Container>
    </AppBar>
  );
};

const useStyles = makeStyles(() => ({
  container: {
    margin: "auto",
    padding: "0",
    "@media (max-width:1120px)": {
      padding: "0 20px",
    },
    "@media (max-width:599px)": {
      padding: "0 15px",
    },
  },
  logo: {
    height: 40,
    marginRight: 10,
  },
  nav: {
    height: "70px",
    padding: "0 30px",
    position: "relative",
  },
  flexContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
}));

export default Navbar;
